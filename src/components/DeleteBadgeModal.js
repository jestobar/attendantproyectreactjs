import React from 'react';
import Modal from './Modal';
function DeleteBadgeModal(props){
    return (
      <Modal isOpen={props.isOpen} isClose={props.isClose}>
        <div className="DeletebadgeModal">
          <h1>Are you sure ?</h1>
          <p>You are bout to delete this badge</p>
          <div>
            <button
              onClick={props.onDeleteBadge}
              className=" btn btn-danger mr-4"
            >
              Delete
            </button>
            <button onClick={props.isClose} className=" btn btn-secondary">
              Cancel
            </button>
          </div>
        </div>
      </Modal>
    );
}

export default DeleteBadgeModal;