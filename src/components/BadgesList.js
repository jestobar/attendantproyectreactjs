import React from 'react';
import { Link } from 'react-router-dom';
import Gravatar from './Gravata';


import './styles/BadgesList.css';



class BadgesListItem extends React.Component {
  render() {
    return (
      <div className="BadgesListItem">
        <Gravatar
          className="BadgesListItem__avatar"
          email={this.props.badge.email}
          alt={`${this.props.badge.firstName} ${this.props.badge.lastName}`}
        />

        <div>
          <strong>
            {this.props.badge.firstName} {this.props.badge.lastName}
          </strong>
          <br />@{this.props.badge.twitter}
          <br />
          {this.props.badge.jobTitle}
        </div>
      </div>
    );
  }
}
function useSerchBadges(badges){
  const [query, setQuery] = React.useState("");
  //react hooks use memo sirve para memorisar lo que busco
  // es decir reliza una vez la accion y la vuvlve a utilizar
  const [filteredBadges, setFilterResults] = React.useState(badges);

  React.useMemo(() => {
    const result = badges.filter((badge) => {
      return `${badge.firstName} ${badge.lastName}`
        .toLowerCase()
        .includes(query.toLowerCase());
    });
    setFilterResults(result);
  }, [badges, query]);

  return {query, setQuery, filteredBadges };

}

function BadgesList (props) {
  const badges = props.badges;

  const { query,setQuery, filteredBadges } = useSerchBadges(badges);

  if (filteredBadges.length === 0) {
    return (
      <div>
        <div className="form-group">
          <label>Filter Badges</label>
          <input
            type="text"
            className="form-control"
            value={query}
            onChange={(e) => {
              setQuery(e.target.value);
            }}
          ></input>
        </div>
        <h3>
          <Link className="btn btn-primary" to="/badges/new">
            Create new Badge
          </Link>
        </h3>
      </div>
    );
  }


    return (
      <div className="BadgesList">
        <div className="form-group">
          <label>Filter Badges</label>
          <input
            type="text"
            className="form-control"
            value={query}
            onChange={(e) => {
              setQuery(e.target.value);
            }}
          ></input>
        </div>
        <ul className="list-unstyled">
          {filteredBadges.map((badge) => {
            return (
              <li key={badge.id}>
                <Link
                  className="text-reset text-decoration-none"
                  to={`badges/${badge.id}/details`}
                >
                  <BadgesListItem badge={badge} />
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    );
}


export default BadgesList;
