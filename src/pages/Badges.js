import React from 'react';
import { Link } from 'react-router-dom';
import Axios from "axios";

import './styles/Badges.css';
import confLogo from '../images/badge-header.svg';
import BadgesList from '../components/BadgesList';
import PageLoading from '../components/pageLoading';
import MiniLoader from '../components/MiniLoader';

class Badges extends React.Component {
  constructor(props) {
    super(props);
    console.log('1. constructor()');

    this.state = {
      loading: true,
      error: null,
      data: undefined,
    };
  }

  componentDidMount() {
    this.buscarData();

    //para que se actulice cierto tiempo se llama pool para que no se vea el loading cada 5 segundos se pone  &&  !this.state.data en la 
    //codicion del load para que solo coja la primera vez
    //para completarlo y que no se genere un buqle hay que limpiar el intervalo en componentwillunmount
    this.intervalId = setInterval(this.buscarData, 5000);
  }
  buscarData = () =>{
    this.setState({loading:true, error:null})
    try{
       console.log("ingreso")
        Axios.get("/Attendants").then((response) => {
          this.setState({ loading: false, data: response.data });
        });
     
      


    }catch(error){
      this.setState({loading:false , error:error })
    }



  }

  componentWillUnmount(){
    clearInterval(this.intervalId);
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('5. componentDidUpdate()');
    console.log({
      prevProps: prevProps,
      prevState: prevState,
    });

    console.log({
      props: this.props,
      state: this.state,
    });
  }

  componentWillUnmount() {
    console.log('6. componentWillUnmount');
    clearTimeout(this.timeoutId);
  }

  render() {
    console.log(this.state.data)
    if (this.state.loading === true && !this.state.data) {
      return <PageLoading></PageLoading>;
    }
    console.log('2/4. render()');
    return (
      <React.Fragment>
        <div className="Badges">
          <div className="Badges__hero">
            <div className="Badges__container">
              <img
                className="Badges_conf-logo"
                src={confLogo}
                alt="Conf Logo"
              />
            </div>
          </div>
        </div>

        <div className="Badges__container">
          <div className="Badges__buttons">
            <Link to="/badges/new" className="btn btn-primary">
              New Badge
            </Link>
          </div>

          <BadgesList badges={this.state.data} />
          {this.state.loading && <MiniLoader></MiniLoader>}
        </div>
      </React.Fragment>
    );
  }
}

export default Badges;
