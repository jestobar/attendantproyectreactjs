import React from 'react';

import './styles/BadgeNew.css';
import header from "../images/platziconf-logo.svg";
import Badge from '../components/Badge';
import BadgeForm from '../components/BadgeForm';
import PageLoading from "../components/pageLoading";
import Axios from "axios";

class BadgeNew extends React.Component {
  state = {
    loading: false,
    error: null,
    messageError: [],
    fila: "",
    form: {
      firstName: "",
      lastName: "",
      email: "",
      jobTitle: "",
      twitter: "",
    },
  };

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };
  createdata = async (e) => {
    e.preventDefault();
    this.setState({ loading: true });
    this.setState({ loading: true, error: null });
    try {
      await Axios.post("/Attendants", this.state.form)
        .then(() => {
          this.setState({ loading: false });
          this.props.history.push("/badges");
        })
        .catch((error) => {
          this.setState({
            loading: false,
            messageError: error.response.data,
            error: error,
          });
          this.errorMesage();
        });
    } catch (e) {
      console.log(e);
    }
  };
  errorMesage = () => {
    if (this.state.error) {
      var count = 0;
      var mesageError = "";
      Object.entries(this.state.messageError).map(([camp, value]) => {
        if (count === 0) {
          console.log();

          mesageError = "Errores :" + " " + value;

          count = 1;
        } else {
          console.log("Segundo");

          mesageError = mesageError + " " + value;
          console.log(mesageError);
        }

        console.log(camp);
      });
      this.setState({ fila: mesageError });
      console.log(this.state.messageError);
    }
  };

  render() {
    if (this.state.loading) {
      return <PageLoading />;
    }
    return (
      <React.Fragment>
        <h1>New Attendant</h1>
        <div className="BadgeNew__hero">
          <img
            className="BadgeNew__hero-image img-fluid"
            src={header}
            alt="Logo"
          />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-6">
              <Badge
                firstName={this.state.form.firstName || "FIRTS_NAME"}
                lastName={this.state.form.lastName || "LAST_NAME"}
                twitter={this.state.form.twitter || "TWITTER"}
                jobTitle={this.state.form.jobTitle || "jOB_TITLE"}
                email={this.state.form.email || "EMAIL"}
                avatarUrl="https://www.gravatar.com/avatar/21594ed15d68ace3965642162f8d2e84?d=identicon"
              />
            </div>

            <div className="col-6">
              <BadgeForm
                onChange={this.handleChange}
                formValues={this.state.form}
                onSubmit={this.createdata}
                errorMessage={this.state.fila}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BadgeNew;
