import React, { Component } from 'react';

import PageLoading from '../components/pageLoading'
import PageError from "../components/PageError";
import Axios from 'axios';
import BadgeDetails from './BadgeDetails';



class BadgeDetailsContainer extends React.Component {
  state = {
    loading: true,
    error: null,
    data: undefined,
    modalIsOpen: false,
  };
  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    this.setState({ loading: true, error: null });
    try {
      await Axios.get(`/Attendants/${this.props.match.params.badgeId}`)
        .then((respuesta) => {
          this.setState({ loading: false, data: respuesta.data });
        })
        .catch((error) => {
          this.setState({ loading: false, error: error });
        });
    } catch (e) {
      console.log(e);
    }
  };

  handleOpenModal = (e) => {
    console.log("ope");
    this.setState({ modalIsOpen: true });
  };
  handleCloseModal = (e) => {
    console.log("cerrado");
    this.setState({ modalIsOpen: false });
  };

  onDeleteBadge =  async (e)=>{
    this.setState({loading:true,error:null});

    try{
      await Axios.delete( `/Attendants/${this.props.match.params.badgeId}`).then(()=>{
        this.props.history.push('/badges')
      }).catch((error)=>{
        this.setState({loading:false, error:error})
        alert(error.message);

      })

    }catch(e){

    }

  };

  render() {
    if (this.state.loading) {
      return <PageLoading></PageLoading>;
    }
    if (this.state.error) {
      return <PageError error={this.state.error}></PageError>;
    }

    return (
      <BadgeDetails
        badge={this.state.data}
        onCloseModal={this.handleCloseModal}
        onOpenModal={this.handleOpenModal}
        modalIsOpen={this.state.modalIsOpen}
        onDeleteBadge={this.onDeleteBadge}
      ></BadgeDetails>
    );
  }
}

export default BadgeDetailsContainer;